Shell script to ease up sound downloads using [youtube-dl](https://youtube-dl.org/) and [ffmpeg](https://ffmpeg.org/) to add metadata.

```
Usage: ./ytdl.sh ARTIST_NAME SONG_TITLE YT_VIDEO_ID
Output: ARTIST_NAME-SONG_TITLE.best-available-audio-format
File Metadata: title=SONG_TITLE, artist=ARTIST_NAME
```
