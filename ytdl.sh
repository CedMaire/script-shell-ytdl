#!/usr/bin/env bash

# Usage: ./ytdl.sh ARTIST_NAME SONG_TITLE YT_VIDEO_ID
# Output: ARTIST_NAME-SONG_TITLE.best-available-audio-format
# File Metadata: title=SONG_TITLE, artist=ARTIST_NAME

ARTIST=$1
TITLE=$2
ID=$3

echo "Downloading video and extracting sound..."

# Download and get file name.
FILE=$(youtube-dl -x --audio-format best --audio-quality 0 -o "$ARTIST-$TITLE.%(ext)s" "$ID" | awk '/ffmpeg/ {print $3}')

# Make a temporary copy (since ffmpeg cannot edit in place... and -y doesn't work...).
mv "$FILE" tmp-"$FILE"

echo "Adding metadata to sound file..."

# Add metadata to output file.
ffmpeg -i tmp-"$FILE" -metadata artist="$ARTIST" -metadata title="$TITLE" "$FILE" > /dev/null 2>&1

# Delete the temporary file again.
rm tmp-"$FILE"

echo "Saved under: $FILE"

